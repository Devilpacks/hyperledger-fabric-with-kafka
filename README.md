## Hyperledger Fabric Network using Kafka Ordering Service

This repository has the required files to create a Hyperledger Fabric network with Kafka-based ordering service

## Introduction

Hyperledger Fabric uses a solo orderer node for testing purposes, while you develop your application and solo is NOT intended for production because it is not, and will never be, fault tolerant. That's why Hyperledger Fabric introduced Kafka as it's primary consensus mechanism among the orderers to use in production to guarantee crash fault tolerance.

## Architecture

The network consists of:
 - 3 Ordering Service Nodes
 - 2 Organizations, 2 peers for each organization, 4 in total
 - 3 Zookeeper nodes for the ZooKeeper ensemble(The number of Zookeeper nodes will either be 3, 5, or 7. It has to be an odd number to avoid split-brain scenarios, and larger than 1 in order to avoid single point of failures. Anything beyond 7 ZooKeeper servers is considered an overkill.)
 - 4 Kafka brokers(The number of Kafka nodes should be set to 4 at a minimum because this is the minimum number of nodes necessary in order to exhibit crash fault tolerance, i.e. with 4 brokers, you can have 1 broker go down, all channels will continue to be writeable and readable, and new channels can be created)

## Technical Documentation

### How to install it
 - First, follow the instructions to grab all the [prerequisites](http://hyperledger-fabric.readthedocs.io/en/release-1.3/prereqs.html) and [samples](https://hyperledger-fabric.readthedocs.io/en/release-1.3/install.html) for running hyperledger fabric.
 - Inside the fabric-samples directory, there is a directory first-network. Clone this repository and move all the files in the first-network directory.
 - In a terminal, move in the first-network directory and type ./byfn.sh down to delete any containers, etc. so there are no problems from previous installations. Then type ./byfn.sh up and the network is up and running and will execute an end-to-end scenario.
 - To bring the network down type ./byfn.sh down

